const express = require('express');
const router = express.Router();
const adm = require('./controllers/admController')

// AUTH ADMIN
router.get('/admin-login', adm.loginPage)
router.post('/admin-login', adm.loginProcess)

// DASHBOARD ADMIN
router.get('/admin-dashboard', adm.mainDashboard)
router.get('/biodata/:id', adm.biodata)
router.get('/create', adm.createForm)
router.post('/create', adm.createProcess)
router.get('/edit-data', adm.updateForm)
router.get('/update/:id', adm.updateItem)
router.post('/update/:id', adm.updateProcess)
router.get('/delete/:id', adm.delete)

module.exports = router;